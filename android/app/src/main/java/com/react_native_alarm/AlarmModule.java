package com.react_native_alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.react_native_alarm.util.SharedPreferencesManager;

public class AlarmModule extends ReactContextBaseJavaModule {
    private static final String REACT_CLASS = "AlarmModule";
    private Context mContext;

    public AlarmModule(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
    }

    @NonNull
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactMethod
    public void setAlarm() {
        AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(mContext, AlarmBroadcastReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(mContext, 0, i, 0);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60 * 3, pi); // Millisec * Second * Minute
        SharedPreferencesManager.getInstance().saveAlarmTime(System.currentTimeMillis() + 1000 * 60 * 3);
    }

    @ReactMethod
    public void cancelAlarm() {
        Intent intent = new Intent(mContext, AlarmBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
