package com.react_native_alarm.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {
    private static final String PREF_NAME = "PREF_CONCAR";
    public static final String PREF_ALARM = "PREF_ALARM";

    private static SharedPreferencesManager sInstance;

    private final SharedPreferences mPreference;

    private SharedPreferencesManager(Context context) {
        mPreference = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferencesManager(context);
        }
    }

    public static synchronized SharedPreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(SharedPreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public void saveAlarmTime(long alarmTime) {
        mPreference.edit().putLong(PREF_ALARM, alarmTime).apply();
    }

    public long getAlarmTime() {
        return mPreference.getLong(PREF_ALARM, 0);
    }
}
