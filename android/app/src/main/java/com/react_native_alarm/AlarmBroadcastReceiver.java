package com.react_native_alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.react_native_alarm.util.SharedPreferencesManager;

public class AlarmBroadcastReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null) {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Intent startServiceIntent = new Intent(context, AlarmService.class);
                context.startService(startServiceIntent);
                Toast.makeText(context, "Restart Service !!!!!!!!!!", Toast.LENGTH_LONG).show();
            }
        }
        Toast.makeText(context, "Alarm React Native !!!!!!!!!!", Toast.LENGTH_LONG).show();
    }
}