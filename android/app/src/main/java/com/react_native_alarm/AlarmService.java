package com.react_native_alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.react_native_alarm.util.SharedPreferencesManager;

public class AlarmService extends Service {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long alarmTime = SharedPreferencesManager.getInstance().getAlarmTime();
        AlarmManager am = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(getBaseContext(), AlarmBroadcastReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(getBaseContext(), 0, i, 0);
        am.set(AlarmManager.RTC_WAKEUP, alarmTime - System.currentTimeMillis(), pi); // Millisec * Second * Minute
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
