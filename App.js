import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import RNAlarm from 'react-native-alarm';
import Button from 'react-native-button'
import AlarmModule from './AlarmModule';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          containerStyle={{ padding: 10, height: 45, width: 200, overflow: 'hidden', borderRadius: 4, backgroundColor: 'white', marginBottom: 20 }}
          style={{ fontSize: 20, color: 'green' }}
          title="Set Alarm"
          onPress={() => {
            AlarmModule.setAlarm();
          }}>
          Set Alarm
        </Button>


        <Button
          containerStyle={{ padding: 10, height: 45, width: 200, overflow: 'hidden', borderRadius: 4, backgroundColor: 'white' }}
          style={{ fontSize: 20, color: 'green' }}
          onPress={() => {
            AlarmModule.cancelAlarm();
          }}>
          Cancel Alarm
        </Button>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
